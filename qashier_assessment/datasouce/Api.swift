//
//  Api.swift
//  qashier_assessment
//
//  Created by Hanif Hasan on 19/07/2023.
//

import Foundation

class Api {
    
    func getCarpark(completionHandler: @escaping (Carpark) -> Void) {
      
        let url = URL(string: "https://api.data.gov.sg/v1/transport/carpark-availability")!
     
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
          if let error = error {
            print("Error with fetching carpark-availability: \(error)")
            return
          }
    
          guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
              print("Error with the response, unexpected status code: \(String(describing: response))")
            return
          }
          
          if let data = data,
            let carpark = try? JSONDecoder().decode(Carpark.self, from: data) {
        
            completionHandler(carpark)
          }
        })
        task.resume()
      }
}
