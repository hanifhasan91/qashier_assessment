//
//  ViewController.swift
//  qashier_assessment
//
//  Created by Hanif Hasan on 18/07/2023.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var lblSmallHighSlot: UILabel!
    @IBOutlet weak var lblSmallHighCarparkNum: UILabel!
    @IBOutlet weak var lblSmallLowSlot: UILabel!
    @IBOutlet weak var lblSmallLowCarparkNum: UILabel!
    
    @IBOutlet weak var lblMediumHighSlot: UILabel!
    @IBOutlet weak var lblMediumHighCarparkNum: UILabel!
    @IBOutlet weak var lblMediumLowSlot: UILabel!
    @IBOutlet weak var lblMediumLowCarparkNum: UILabel!
    
    @IBOutlet weak var lblBigHighSlot: UILabel!
    @IBOutlet weak var lblBigHighCarparkNum: UILabel!
    @IBOutlet weak var lblBigLowSlot: UILabel!
    @IBOutlet weak var lblBigLowCarparkNum: UILabel!
    
    @IBOutlet weak var lblLargeHighSlot: UILabel!
    @IBOutlet weak var lblLargeHighCarparkNum: UILabel!
    @IBOutlet weak var lblLargeLowSlot: UILabel!
    @IBOutlet weak var lblLargeLowCarparkNum: UILabel!
    
    var timerCount = 0
    var carpark : Carpark?
    var carparkDatas = [CarparkData]()
    var carparkDataSmall = [CarparkData]()
    var carparkDataMedium = [CarparkData]()
    var carparkDataBig = [CarparkData]()
    var carparkDataLarge = [CarparkData]()
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        loadTimer()
    }
    
    
    func loadTimer() {
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
            self.timerCount += 1
            if self.timerCount == 60 {
                self.timerCount = 0
                self.loadData()
            }
        }
    }

    func loadData () {
        Api().getCarpark { (data) in
            DispatchQueue.main.async{
                self.carpark = data
                self.carparkDatas = data.items[0].carparkData
                if self.carparkDatas.count != 0 {
                    self.displayData()
                }
                
            }
           
        }
    }
    
    func displayData() {
  
        carparkDataSmall =  carparkDatas[0].getCarparkDataOnCategory(type: 1, carparkDatas: carparkDatas)
        carparkDataMedium =  carparkDatas[0].getCarparkDataOnCategory(type: 2, carparkDatas: carparkDatas)
        carparkDataBig =  carparkDatas[0].getCarparkDataOnCategory(type: 3, carparkDatas: carparkDatas)
        carparkDataLarge =  carparkDatas[0].getCarparkDataOnCategory(type: 4, carparkDatas: carparkDatas)
        
        let smallHighest =  carparkDatas[0].getCarparkHeighest(carparkDatas: carparkDataSmall)
        let smallHighCarparkNum =  carparkDatas[0].getCarparkNumberOnCategorySizeLots(sizeSlot: smallHighest, carparkDatas: carparkDataSmall)
        let smallLowest =  carparkDatas[0].getCarparkLowest(carparkDatas: carparkDataSmall)
        let smallLowCarparkNum =  carparkDatas[0].getCarparkNumberOnCategorySizeLots(sizeSlot: smallLowest, carparkDatas: carparkDataSmall)
        
        
        let mediumHighest =  carparkDatas[0].getCarparkHeighest(carparkDatas: carparkDataMedium)
        let mediumHighCarparkNum =  carparkDatas[0].getCarparkNumberOnCategorySizeLots(sizeSlot: mediumHighest, carparkDatas: carparkDataMedium)
        let mediumLowest =  carparkDatas[0].getCarparkLowest(carparkDatas: carparkDataMedium)
        let mediumLowCarparkNum =  carparkDatas[0].getCarparkNumberOnCategorySizeLots(sizeSlot: mediumLowest, carparkDatas: carparkDataMedium)
    
        
        let bigHighest =  carparkDatas[0].getCarparkHeighest(carparkDatas: carparkDataBig)
        let bigHighCarparkNum =  carparkDatas[0].getCarparkNumberOnCategorySizeLots(sizeSlot: bigHighest, carparkDatas: carparkDataBig)
        let bigLowest =  carparkDatas[0].getCarparkLowest(carparkDatas: carparkDataBig)
        let bigLowCarparkNum =  carparkDatas[0].getCarparkNumberOnCategorySizeLots(sizeSlot: bigLowest, carparkDatas: carparkDataBig)
        
        
        let largeHighest =  carparkDatas[0].getCarparkHeighest(carparkDatas: carparkDataLarge)
        let largeHighCarparkNum =  carparkDatas[0].getCarparkNumberOnCategorySizeLots(sizeSlot: largeHighest, carparkDatas: carparkDataLarge)
        let largeLowest =  carparkDatas[0].getCarparkLowest(carparkDatas: carparkDataLarge)
        let largeLowCarparkNum =  carparkDatas[0].getCarparkNumberOnCategorySizeLots(sizeSlot: largeLowest, carparkDatas: carparkDataLarge)
   
        lblSmallHighSlot.text = "HIGHEST (\(smallHighest) lots available) "
        lblSmallHighCarparkNum.text = "\(smallHighCarparkNum)"
        lblSmallLowSlot.text = "LOWEST (\(smallLowest) lots available) "
        lblSmallLowCarparkNum.text = "\(smallLowCarparkNum)"
        
        lblMediumHighSlot.text = "HIGHEST (\(mediumHighest) lots available) "
        lblMediumHighCarparkNum.text = "\(mediumHighCarparkNum)"
        lblMediumLowSlot.text = "LOWEST (\(mediumLowest) lots available) "
        lblMediumLowCarparkNum.text = "\(mediumLowCarparkNum)"
        
        lblBigHighSlot.text = "HIGHEST (\(bigHighest) lots available) "
        lblBigHighCarparkNum.text = "\(bigHighCarparkNum)"
        lblBigLowSlot.text = "LOWEST (\(bigLowest) lots available) "
        lblBigLowCarparkNum.text = "\(bigLowCarparkNum)"
        
        lblLargeHighSlot.text = "HIGHEST (\(largeHighest) lots available) "
        lblLargeHighCarparkNum.text = "\(largeHighCarparkNum)"
        lblLargeLowSlot.text = "LOWEST (\(largeLowest) lots available) "
        lblLargeLowCarparkNum.text = "\(largeLowCarparkNum)"
        

    }

   
}

