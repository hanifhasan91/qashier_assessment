//
//  CarparkModel.swift
//  qashier_assessment
//
//  Created by Hanif Hasan on 19/07/2023.
//

import Foundation

struct Carpark: Codable {
    let items: [Item]
}


struct Item: Codable {
    let timestamp: String
    let carparkData: [CarparkData]

    enum CodingKeys: String, CodingKey {
        case timestamp
        case carparkData = "carpark_data"
    }
}



struct CarparkData: Codable {
    let carparkInfo: [CarparkInfo]
    let carparkNumber, updateDatetime: String

    enum CodingKeys: String, CodingKey {
        case carparkInfo = "carpark_info"
        case carparkNumber = "carpark_number"
        case updateDatetime = "update_datetime"
    }
    
    func sumTotalLots() -> String {
        let totalLots = carparkInfo.map({Int($0.totalLots) ?? 0}).reduce(0, +)
        return "\(totalLots)"
    }
    
    func sumLotsAvailable() -> String {
        let lotsAvailable = carparkInfo.map({Int($0.lotsAvailable) ?? 0}).reduce(0, +)
        return "\(lotsAvailable)"
    }
    
    func getCarparkDataOnCategory(type : Int , carparkDatas : [CarparkData] ) -> [CarparkData] {
        var objects = [CarparkData]()
        switch type {
        case 1:
            return carparkDatas.filter{ (Int($0.sumTotalLots()) ?? 0) < 100 }
        case 2:
            return carparkDatas.filter{ (Int($0.sumTotalLots()) ?? 0) >= 100  && (Int($0.sumTotalLots()) ?? 0) < 300}
        case 3:
            return carparkDatas.filter{ (Int($0.sumTotalLots()) ?? 0) >= 300  && (Int($0.sumTotalLots()) ?? 0) < 400}
        case 4:
            return carparkDatas.filter{ (Int($0.sumTotalLots()) ?? 0) >= 400 }
        default:
            return []
        }
    }
    
    func getCarparkHeighest(carparkDatas : [CarparkData] ) -> Int {
        var array : [Int] = []
        
        for item in carparkDatas {
            array.append(Int(item.sumLotsAvailable()) ?? 0)
        }
        return array.max()  ?? 0
    }
    
    func getCarparkLowest(carparkDatas : [CarparkData] ) -> Int {
        
        var array : [Int] = []
        
        for item in carparkDatas {
            array.append(Int(item.sumLotsAvailable()) ?? 0)
        }
        return array.min()  ?? 0
    }
   
    func getCarparkNumberOnCategorySizeLots( sizeSlot : Int , carparkDatas : [CarparkData] ) -> String {
        var carparkNumber = ""
        var objects =  carparkDatas.filter{ (Int($0.sumLotsAvailable()) == sizeSlot) }
        carparkNumber = objects.map{String($0.carparkNumber)}.joined(separator: ",")
        return carparkNumber
    }
}


struct CarparkInfo: Codable {
    let totalLots: String
    let lotType: String
    let lotsAvailable: String

    enum CodingKeys: String, CodingKey {
        case totalLots = "total_lots"
        case lotType = "lot_type"
        case lotsAvailable = "lots_available"
    }
}
